<?php
$this->breadcrumbs = array(
    'Manage Posts',
);
?>
<h1>Manage Posts</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'username',
        'email',
        'homepage',
        'ip',
        'browser',
        array(
            'name' => 'create_time',
            'type' => 'date',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>

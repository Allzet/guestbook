<div id="list">
    <?php
    Yii::import('ext.yii-ckeditor.CKEditorWidget');

//    $js = Yii::app()->getClientScript();
//    $js->registerScriptFile('//cdn.ckeditor.com/4.4.7/basic/ckeditor.js');
//    $js->registerScript('js2', "CKEDITOR.replace('Post_text',{width:510});", CClientScript::POS_LOAD);

    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
        'template' => "{items}\n{pager}",
    ));
    ?>

    <?php if (!empty($model)): ?>
        <div class="form">

            <hr />
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'post-form',
                'enableAjaxValidation' => true,
            ));
            ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo CHtml::errorSummary($model); ?>

            <div class="row">
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username', array('size' => 69, 'maxlength' => 128)); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('size' => 69, 'maxlength' => 128)); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'homepage'); ?>
                <?php echo $form->textField($model, 'homepage', array('size' => 69, 'maxlength' => 128)); ?>
                <?php echo $form->error($model, 'homepage'); ?>
            </div>

            <div id="text" class="row">
                <?php
                echo $form->labelEx($model, 'text');
                $this->widget('CKEditorWidget', array(
                    'model' => $model,
                    'attribute' => 'text',
                    'id' => 'text',
                    // editor options http://docs.ckeditor.com/#!/api/CKEDITOR.config
                    'config' => array(
                        'language' => 'ru',
                        'width' => '510',
                    ),
                ));
                ?>

            </div>

            <?php 
           
            if (CCaptcha::checkRequirements()): ?>

                <div class="row">
                    <?php echo $form->labelEx($model, 'verifyCode'); ?>
                    <div>
                        <?php $this->widget('CCaptcha'); ?>
                        <?php // $this->widget('CaptchaExtendedAction'); ?>
                        <?php echo $form->textField($model, 'verifyCode'); ?>
                        <?php echo $form->error($model, 'verifyCode'); ?>
                    </div>
                    <div class="hint">Please enter the letters as they are shown in the image above.
                        <br/>Letters are not case-sensitive.</div>

                <?php endif; ?>
            </div>
            <div id="button_create" class="row buttons">
                <?php
                echo CHtml::ajaxSubmitButton('AddPost', Yii::app()->createUrl('post/index'), array(
                    'type' => 'POST',
                    'update' => '#list',
                        ), array(
                    'type' => 'submit',
                    'id' => 'button_create',
                    'name' => 'Submit'
                ));
                ?> 
            </div>

            <?php
            $this->endWidget();
            echo CHtml::button('PreviewPost', array('id' => 'button_preview'));
            ?>
            <hr />

        </div><!-- form -->

        <div id="preview">

        </div>
        <script>
            $(function () {

                $(document).on('click', '#button_preview', function (e) {
                    e.preventDefault();
                    var texteditor = CKEDITOR.instances['Post_text'].getData();
                    document.getElementById('Post_text').value = texteditor
                    var form = $('#post-form');
                    console.log(form.serialize());
                    $.ajax({
                        type: "POST",
                        url: form.attr('action'),
                        data: form.serialize() + '&preview=true',
                        success: function (msg) {
                            $('#preview').html(msg);

                        }
                    });
                });
            });
        </script>
        <script>
            $(function () {

                $(document).on('focus', '#button_create', function (u) {
                    u.preventDefault();
                    var texteditor = CKEDITOR.instances['Post_text'].getData();
                    document.getElementById('Post_text').value = texteditor
                    var form = $('#post-form');
                    console.log(form.serialize());

                });
            });
        </script>
    <?php endif; ?>
</div>
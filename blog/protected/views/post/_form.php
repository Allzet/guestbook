<div class="form">



    <?php
    Yii::import('ext.yii-ckeditor.CKEditorWidget');


    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo CHtml::errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'username'); ?>
        <?php echo $form->textField($model, 'username', array('size' => 69, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('size' => 69, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'homepage'); ?>
        <?php echo $form->textField($model, 'homepage', array('size' => 69, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'homepage'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php
        $this->widget('CKEditorWidget', array(
            'model' => $model,
            'attribute' => 'text',
            // editor options http://docs.ckeditor.com/#!/api/CKEDITOR.config
            'config' => array(
                'language' => 'ru',
            'width' => '200px',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'text'); ?>
    </div>
    <?php // if($model->isNewRecord){?>
    <?php if (CCaptcha::checkRequirements()): ?>

        <div class="row">

            <div>
                <?php echo $form->labelEx($model, 'verifyCode'); ?>
                <?php $this->widget('CCaptcha'); ?>
                <?php // $this->widget('CaptchaExtendedAction'); ?>
                <?php echo $form->textField($model, 'verifyCode'); ?>
                <?php echo $form->error($model, 'verifyCode'); ?>
            </div>
            <div class="hint">Please enter the letters as they are shown in the image above.
                <br/>Letters are not case-sensitive.</div>

        <?php endif; ?>
    </div>
    <?php // }?>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php

/**
 * The followings are the available columns in table 'tbl_post':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $tags
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $author_id
 */
class Post extends CActiveRecord {

    public $verifyCode;
    public $textarea;

    /**
     * Returns the static model of the specified AR class.
     * @return static the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{post}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, text, email', 'required'),
            array('username, homepage', 'length', 'max' => 128),
            array('email', 'email'),
            array('username', 'safe', 'on' => 'search'),
            array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'username' => 'Username',
            'text' => 'Text',
            'create_time' => 'Create Date',
            'email' => 'E-mail',
            'homepage' => 'Home Page',
            'verifyCode' => 'Verification Code',
            'ip' => 'IP',
            'browser' => 'Browser',
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl() {
        return Yii::app()->createUrl('post/view', array(
                    'id' => $this->id,
        ));
    }

    protected function afterFind() {
        parent::afterFind();
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        return new CActiveDataProvider('Post', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'create_time DESC',
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage'],
            ),
        ));
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->isNewRecord)
                $this->create_time = time();
            $this->ip = $_SERVER['REMOTE_ADDR'];
            $this->browser = $_SERVER['HTTP_USER_AGENT'];
            return true;
        } else {
            return false;
        };
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        parent::afterDelete();
    }

}
